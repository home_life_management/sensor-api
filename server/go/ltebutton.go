package server

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/home_life_management/common_lib/service/database"
)

type ButtonLogger interface {
	AddLteButtonLog(serial string, pushType database.ButtonPushType) error
	Connect() error
	Close()
}

func AddLteButtonLog(w http.ResponseWriter, r *http.Request) {
	// TODO: DB関連structは初期化処理の中で生成
	o := database.DatabaseOption{
		Name:     "life_management",
		User:     "manager",
		Password: "mypassword",
	}
	p, err := database.NewPostgresService(&o)
	if err != nil {
		log.Print(err.Error())
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	body := buf.String()
	var l database.ButtonPushLog
	err = json.Unmarshal([]byte(body), &l)
	if err != nil {
		log.Print(err.Error())
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = p.Connect()
	if err != nil {
		log.Print(err.Error())
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = p.AddLteButtonLog(l)
	if err != nil {
		log.Print(err.Error())
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	p.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}
