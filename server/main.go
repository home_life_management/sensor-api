package main

import (
	"log"
	"net/http"

	server "gitlab.com/home_life_management/sensor-api/server/go"
)

func main() {
	log.Printf("Server started")

	router := server.NewRouter()

	log.Fatal(http.ListenAndServe(":80", router))
}
